/*This file is part of Hardware programming 2016.

    Hardware programming 2016 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Hardware programming 2016 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Hardware programming 2016.  If not, see <http://www.gnu.org/licenses/>. */

#include <avr/pgmspace.h>
#include "hmi_msg.h"

const char january[] PROGMEM = "January";
const char february[] PROGMEM = "February";
const char march[] PROGMEM = "March";
const char april[] PROGMEM = "April";
const char may[] PROGMEM = "May";
const char june[] PROGMEM = "June";

PGM_P const name_month[] PROGMEM = {january, february, march, april, may, june};

